#pragma once

template <typename T>
class Vector2
{
public:
	Vector2<T>();
	Vector2<T>(T = 0, T = 0);

	auto operator + (const Vector2<T>&) const -> const Vector2<T>;

	T x = 0;
	T y = 0;

	static const Vector2<T> zero;
	static const Vector2<T> one;
};

template <typename T>
class Vector3
{
public:
	Vector3<T>();
	Vector3<T>(T = 0, T = 0, T = 0);

	auto operator + (const Vector3<T>&) const -> const Vector3<T>;

	T x = 0;
	T y = 0;
	T z = 0;

	static const Vector3<T> zero;
	static const Vector3<T> one;
};

template <typename T>
class Vector4
{
public:
	Vector4<T>();
	Vector4<T>(T = 0, T = 0, T = 0, T = 0);

	auto operator + (const Vector4<T>&) const -> const Vector4<T>;

	T x = 0;
	T y = 0;
	T z = 0;
	T w = 0;

	static const Vector4<T> zero;
	static const Vector4<T> one;
};

template <typename T>
class Color4
{
public:
	Color4<T>();
	Color4<T>(T r, T g, T b, T a = 1.0);

	auto operator + (const Color4<T>&) const -> const Color4<T>;
	auto operator * (const Color4<T>&) const -> const Color4<T>;

	T r = 0;
	T g = 0;
	T b = 0;
	T a = 0;

	static const Color4<T> white;
	static const Color4<T> black;
	static const Color4<T> red;
	static const Color4<T> green;
	static const Color4<T> blue;
};

#include "Types.inl"

typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;
typedef Vector2<double> Vector2d;

typedef Vector3<float> Vector3f;
typedef Vector3<int> Vector3i;
typedef Vector3<double> Vector3d;

typedef Vector4<float> Vector4f;
typedef Vector4<int> Vector4i;
typedef Vector4<double> Vector4d;

typedef Color4<float> Color4f;