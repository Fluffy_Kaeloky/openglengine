#include "AssetManager.h"

std::vector<Asset*> AssetManager::assets = std::vector<Asset*>();

auto AssetManager::CreateAsset(AssetType type, void* data, void* metadata) -> Asset *
{
	Asset* asset = new Asset();
	AddAsset(asset);
	return asset;
}

auto AssetManager::CreateAssetFromFile(AssetType type, const std::string & file) -> Asset *
{
	throw new std::exception("This function is not implemented yet.");
}

auto AssetManager::AddAsset(Asset* asset) -> Asset*
{
	for each (auto&& a in assets)
	{
		if (a->GetId() == asset->GetId())
			return nullptr;
	}

	assets.push_back(asset);

	return asset;
}

auto AssetManager::GetAssetFromUUID(UUID uuid) -> Asset *
{
	for each (auto& a in assets)
	{
		if (a->GetId() == uuid)
			return a;
	}
	return nullptr;
}
