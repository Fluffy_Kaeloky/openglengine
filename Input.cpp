#include "Input.h"
#include "Device.h"

#include <GLFW\glfw3.h>

auto Input::Initialize(Device* device) -> int
{
	if (!initialized)
	{
		this->device = device;

		glfwSetKeyCallback(device->GetWindow(), Input::KeyCallback);

		initialized = true;
		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}

auto Input::Shutdown() -> void
{
	if (initialized)
	{
		device = nullptr;
		initialized = false;
	}
}

auto Input::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) -> void
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}
