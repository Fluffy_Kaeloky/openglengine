#pragma once
#include "Asset.h"
#include <GL\glew.h>

enum class ShaderType
{
	SHADER_VERTEX,
	SHADER_FRAGMENT
};

class AssetShader:
	public Asset
{
public:
	AssetShader(const char* source, ShaderType type);
	virtual ~AssetShader();

private:
};

struct ShaderData
{
	ShaderData(const char* source);
	~ShaderData();

	char* source = nullptr;
};

struct ShaderMetadata
{
	~ShaderMetadata();

	GLuint index = 0;
	GLint compileSuceeded = 0;
	char compilationLog[512] = "\0";
	ShaderType type = ShaderType::SHADER_FRAGMENT;
};