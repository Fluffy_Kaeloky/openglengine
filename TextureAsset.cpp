#include "TextureAsset.h"
#include <SOIL.h>

TextureAsset::TextureAsset(const std::string& filename)
{
	metadata = new TextureMetadata();

	unsigned char* datas = SOIL_load_image(filename.c_str(), &((TextureMetadata*)metadata)->width, &((TextureMetadata*)metadata)->height, 0, SOIL_LOAD_RGB);
	data = new TextureData(datas, ((TextureMetadata*)metadata)->width * ((TextureMetadata*)metadata)->height * 3);
	SOIL_free_image_data(datas);

	glGenTextures(1, &((TextureMetadata*)metadata)->texId);
	glBindTexture(GL_TEXTURE_2D, ((TextureMetadata*)metadata)->texId);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ((TextureMetadata*)metadata)->width, ((TextureMetadata*)metadata)->height, 0, GL_RGB, GL_UNSIGNED_BYTE, ((TextureData*)data)->data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
}

TextureAsset::~TextureAsset()
{
	delete data;
	delete metadata;
}

auto TextureAsset::SetFilter(TextureFilterType type, TextureFilter filter) -> void
{
	switch (type)
	{
	case TextureFilterType::MIGNIFICATION:
		((TextureMetadata*)metadata)->minFilter = filter;
		break;
	case TextureFilterType::MAGNIFICATION:
		((TextureMetadata*)metadata)->magFilter = filter;
		break;
	}
}

TextureData::TextureData(unsigned char* data, unsigned int size)
{
	this->data = (unsigned char*)malloc(size);
	memcpy(this->data, data, size);
}

TextureData::~TextureData()
{
	free(data);
}

TextureMetadata::~TextureMetadata()
{
	if (texId != 0)
		glDeleteTextures(1, &texId);
	texId = 0;
}
