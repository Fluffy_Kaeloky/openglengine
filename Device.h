#pragma once

#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <string>
////Test includes
////End tests

////Tests
class MeshRenderer;
////End tests

class Input;
class Renderer;

class Device
{
public:
	Device() = default;
	~Device();

	auto Initialize(int width, int height, const std::string& title) -> int;
	auto Shutdown() -> void;

	auto Run() -> void;

	inline auto GetWindow() const -> GLFWwindow* { return window; }

private:
	bool initialized = false;
	GLFWwindow* window = nullptr;

	Input* input = nullptr;
	Renderer* renderer = nullptr;

	////Tests
private:
	MeshRenderer* meshRenderer = nullptr;
	////End tests
};
