#include "ModelAsset.h"
#include <iostream>

ModelAsset::ModelAsset(float* vertices, int vertexCount, int vertexSize, int stride) :
	Asset()
{
	data = new ModelData(vertices, sizeof(float) * vertexSize * vertexCount);
	metadata = new ModelMetadata();
	((ModelMetadata*)metadata)->vertexCount = vertexCount;
	((ModelMetadata*)metadata)->vertexSize = vertexSize;
	((ModelMetadata*)metadata)->stride = stride;

	glGenBuffers(1, &((ModelMetadata*)metadata)->vbo);
	glGenVertexArrays(1, &((ModelMetadata*)metadata)->vao);

	glBindBuffer(GL_ARRAY_BUFFER, ((ModelMetadata*)metadata)->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexSize * vertexCount, ((ModelData*)data)->vertex, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	type = AssetType::ASSET_MODEL;
}

ModelAsset::~ModelAsset()
{
	delete GetData();
	delete GetMetadata();
}

auto ModelAsset::SetAttribute(GLuint layout, GLuint attribSize, GLenum type, GLint offset, GLint stride, bool normalize) -> void
{
	glBindVertexArray(((ModelMetadata*)metadata)->vao);
	glBindBuffer(GL_ARRAY_BUFFER, ((ModelMetadata*)metadata)->vbo);

	GLint typeSize = 1;
	switch (type)
	{
	case GL_FLOAT:
		typeSize = sizeof(GLfloat);
		break;
	case GL_DOUBLE:
		typeSize = sizeof(GLdouble);
		break;
	case GL_INT:
		typeSize = sizeof(GLint);
		break;
	case GL_UNSIGNED_INT:
		typeSize = sizeof(GLuint);
	default:
		std::cout << "(SetAttribute) type not supported." << std::endl;
	}
	
	glVertexAttribPointer(layout, attribSize, type, normalize ? GL_TRUE : GL_FALSE, stride * typeSize, (GLvoid*)(offset * typeSize));
	glEnableVertexAttribArray(layout);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

auto ModelAsset::SetIndexArray(unsigned int* indices, unsigned int count) -> void
{
	((ModelData*)data)->indices = (float*)malloc(sizeof(unsigned int) * count);
	memcpy(((ModelData*)data)->indices, indices, sizeof(unsigned int) * count);

	ModelMetadata* meta = (ModelMetadata*)metadata;
	meta->indexCount = count;
	glBindVertexArray(meta->vao);

	if (meta->ebo == 0)
		glGenBuffers(1, &meta->ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meta->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * count, indices, GL_STATIC_DRAW);
	glBindVertexArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

ModelData::ModelData(float* vertex, int byteSize)
{
	this->vertex = (float*)malloc(byteSize);
	memcpy(this->vertex, vertex, byteSize);
}

ModelData::~ModelData()
{
	if (vertex)
		free(vertex);
	vertex = nullptr;

	if (indices)
		free(indices);
	indices = nullptr;
}

ModelMetadata::~ModelMetadata()
{
	glDeleteBuffers(1, &vbo);
	if (ebo != 0)
		glDeleteBuffers(1, &ebo);

	glDeleteVertexArrays(1, &vao);
}
