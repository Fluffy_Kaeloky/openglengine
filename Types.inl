#include "Types.h"
#include <algorithm>

template<typename T>
inline Vector2<T>::Vector2()
{
}

template<typename T>
inline Vector2<T>::Vector2(T x, T y)
{
	this->x = x;
	this->y = y;
}

template<typename T>
inline auto Vector2<T>::operator+(const Vector2<T>& other) const -> const Vector2<T>
{
	return Vector2<T>(x + other.x, y + other.y);
}

template<typename T>
inline Vector3<T>::Vector3()
{
}

template<typename T>
inline Vector3<T>::Vector3(T x, T y, T z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

template<typename T>
inline auto Vector3<T>::operator+(const Vector3<T>& other) const -> const Vector3<T>
{
	return Vector3<T>(x + other.x, y + other.y, z + other.z);
}

template<typename T>
inline Vector4<T>::Vector4()
{
}

template<typename T>
inline Vector4<T>::Vector4(T x, T y, T z, T w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

template<typename T>
inline auto Vector4<T>::operator+(const Vector4<T>& other) const -> const Vector4<T>
{
	return Vector4<T>(x + other.x, y + other.y, z + other.z, w + other.w);
}

template<typename T>
inline Color4<T>::Color4()
{
	a = 1.0;
}

template<typename T>
inline Color4<T>::Color4(T r, T g, T b, T a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

template<typename T>
inline auto Color4<T>::operator+(const Color4<T>& other) const -> const Color4<T>
{
	return Color4<T>(std::max<T>(std::min<T>(r + other.r, 1.0f), -1.0f),
		std::max<T>(std::min<T>(g + other.g, 1.0f), -1.0f),
		std::max<T>(std::min<T>(b + other.b, 1.0f), -1.0f),
		std::max<T>(std::min<T>(a + other.a, 1.0f), -1.0f));
}

template<typename T>
inline auto Color4<T>::operator*(const Color4<T>& other) const -> const Color4<T>
{
	return Color4<T>(std::max<T>(std::min<T>(r * other.r, 1.0f), -1.0f), 
		std::max<T>(std::min<T>(g * other.g, 1.0f), -1.0f),
		std::max<T>(std::min<T>(b * other.b, 1.0f), -1.0f),
		std::max<T>(std::min<T>(a * other.a, 1.0f), -1.0f));
}

template <typename T>
const Vector2<T> Vector2<T>::zero = Vector2<T>(0.0, 0.0);
template <typename T>
const Vector2<T> Vector2<T>::one = Vector2<T>(1.0, 1.0);

template <typename T>
const Vector3<T> Vector3<T>::zero = Vector3<T>(0.0, 0.0, 0.0);
template <typename T>
const Vector3<T> Vector3<T>::one = Vector3<T>(1.0, 1.0, 1.0);

template <typename T>
const Vector4<T> Vector4<T>::zero = Vector4<T>(0.0, 0.0, 0.0, 0.0);
template <typename T>
const Vector4<T> Vector4<T>::one = Vector4<T>(1.0, 1.0, 1.0, 1.0);

template<typename T>
const Color4<T> Color4<T>::white = Color4<T>(1.0, 1.0, 1.0, 1.0);
template<typename T>
const Color4<T> Color4<T>::black = Color4<T>(0.0, 0.0, 0.0, 1.0);
template<typename T>
const Color4<T> Color4<T>::red = Color4<T>(1.0, 0.0, 0.0, 1.0);
template<typename T>
const Color4<T> Color4<T>::green = Color4<T>(0.0, 1.0, 0.0, 1.0);
template<typename T>
const Color4<T> Color4<T>::blue = Color4<T>(0.0, 0.0, 1.0, 1.0);
