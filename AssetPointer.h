#pragma once
#include "Asset.h"

class AssetPointer
{
public:
	auto operator -> () const -> Asset*;
	auto operator * () const -> Asset*;

	auto operator = (Asset* other) -> const AssetPointer&;
	auto operator = (const AssetPointer& other) -> const AssetPointer&;

	auto Set(Asset* other) -> void;

private:
	auto Resolve() -> void;
	auto Resolve() const -> Asset*;

private:
	UUID uuid;
	bool resolved = false;
	Asset* resolvedPointer = nullptr;
};