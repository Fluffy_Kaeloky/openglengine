#pragma once
#include "Asset.h"
#include "TextureAsset.h"
#include "AssetPointer.h"
#include "Types.h"

class MaterialAsset:
	public Asset
{
public:
	MaterialAsset();
	virtual ~MaterialAsset();

	auto SetColor(const Color4f& newColor) -> void;
	auto SetTexture(TextureAsset* tex) -> void;
	auto SetRepeatMode(TextureRepeatMode mode) -> void;
};

struct MaterialData
{
	Color4f color = Color4f::white;
	AssetPointer texture;
};

struct MaterialMetadata
{
	TextureRepeatMode repeatMode = TextureRepeatMode::REPEAT;
};