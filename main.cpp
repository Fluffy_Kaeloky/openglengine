#include <stdlib.h>

#include "Device.h"

auto main(int argc, char* argv[]) -> int
{
	Device device;
	device.Initialize(800, 600, "Title");

	device.Run();

	device.Shutdown();

	return EXIT_SUCCESS;
}