#include "MaterialAsset.h"

MaterialAsset::MaterialAsset()
{
	data = new MaterialData();
	metadata = new MaterialMetadata();
}

MaterialAsset::~MaterialAsset()
{
	delete data;
	delete metadata;
}

auto MaterialAsset::SetColor(const Color4f& newColor) -> void
{
	((MaterialData*)data)->color = newColor;
}

auto MaterialAsset::SetTexture(TextureAsset* tex) -> void
{
	((MaterialData*)data)->texture = tex;
}

auto MaterialAsset::SetRepeatMode(TextureRepeatMode mode) -> void
{
	((MaterialMetadata*)metadata)->repeatMode = mode;
}
