#pragma once

#include <rpc.h>
#include <string>

enum class AssetType
{
	ASSET_MODEL,
	ASSET_TEXTURE,
	ASSET_SHADER,
	ASSET_OTHER
};

class Asset
{
public:
	Asset();
	Asset(void* data, void* metadata, AssetType type);
	virtual ~Asset() = default;

	inline auto GetId() const -> UUID { return uuid; }

	auto GetData() const -> void* { return data; }
	auto GetMetadata() const -> void* { return metadata; }

protected:
	void* data = nullptr;
	void* metadata = nullptr;
	AssetType type = AssetType::ASSET_OTHER;

private:
	UUID uuid;
};
