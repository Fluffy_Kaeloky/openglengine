#include "Asset.h"

Asset::Asset()
{
	UuidCreate(&uuid);
}

Asset::Asset(void* data, void* metadata, AssetType type):
	Asset()
{
	this->data = data;
	this->metadata = metadata;
	this->type = type;
}
