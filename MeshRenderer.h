#pragma once

#include "Drawable.h"
#include "AssetPointer.h"
#include "MaterialAsset.h"
#include "Types.h"
#include <GL\glew.h>

class MeshRenderer:
	public Drawable
{
public:
	MeshRenderer();

	inline auto GetMeshAsset() const -> const AssetPointer& { return meshData; }
	auto SetMesh(Asset* asset) -> void;

	auto SetMaterial(MaterialAsset* asset) -> void;
	inline auto GetMaterial() const -> const AssetPointer& { return material; }

	auto SetVertexShader(Asset* asset) -> void;
	auto SetFragmentShader(Asset* asset) -> void;

	inline auto SetWireframe(bool value) -> void { wireframe = value; }
	inline auto GetWireframe() const -> bool { return wireframe; }

private:
	virtual auto draw() const -> void override;
	auto UpdateShaders() -> void;

private:
	AssetPointer meshData;
	AssetPointer material;

	AssetPointer vertexShader;
	AssetPointer fragmentShader;

	GLuint programIndex = 0;

	bool wireframe = false;
};