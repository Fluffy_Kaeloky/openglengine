#include "MeshRenderer.h"
#include "ModelAsset.h"
#include "AssetShader.h"
#include "GlError.h"
#include <iostream>

MeshRenderer::MeshRenderer()
{
	programIndex = glCreateProgram();
}

auto MeshRenderer::SetMesh(Asset* asset) -> void
{
	meshData.Set(asset);
}

auto MeshRenderer::SetMaterial(MaterialAsset* asset) -> void
{
	material.Set(asset);
}

auto MeshRenderer::SetVertexShader(Asset* asset) -> void
{
	AssetShader* shaderAsset = (AssetShader*)asset;

	if (((ShaderMetadata*)shaderAsset->GetMetadata())->compileSuceeded && ((ShaderMetadata*)shaderAsset->GetMetadata())->type == ShaderType::SHADER_VERTEX)
	{
		glAttachShader(programIndex, ((ShaderMetadata*)shaderAsset->GetMetadata())->index);
		vertexShader.Set(asset);
	}
	else if (((ShaderMetadata*)shaderAsset->GetMetadata())->type != ShaderType::SHADER_VERTEX)
		throw new std::exception("Shader not supported");
	else
		std::cout << "Shader failed compilation ! Log : " << ((ShaderMetadata*)shaderAsset->GetMetadata())->compilationLog << std::endl;

	if (*vertexShader != nullptr && *fragmentShader != nullptr)
		UpdateShaders();
}

auto MeshRenderer::SetFragmentShader(Asset* asset) -> void
{
	AssetShader* shaderAsset = (AssetShader*)asset;

	if (((ShaderMetadata*)shaderAsset->GetMetadata())->compileSuceeded && ((ShaderMetadata*)shaderAsset->GetMetadata())->type == ShaderType::SHADER_FRAGMENT)
	{
		glAttachShader(programIndex, ((ShaderMetadata*)shaderAsset->GetMetadata())->index);
		fragmentShader.Set(asset);
	}
	else if (((ShaderMetadata*)shaderAsset->GetMetadata())->type != ShaderType::SHADER_FRAGMENT)
		throw new std::exception("Shader not supported");
	else
		std::cout << "Shader failed compilation ! Log : " << ((ShaderMetadata*)shaderAsset->GetMetadata())->compilationLog << std::endl;

	if (*vertexShader != nullptr && *fragmentShader != nullptr)
		UpdateShaders();
}

/*auto MeshRenderer::SetColor(const Color4f & newColor) -> void
{
	color = newColor;
	if (programIndex != 0 && *vertexShader != nullptr && *fragmentShader != nullptr)
	{
		GLint vertexColorLocation = glGetUniformLocation(programIndex, "materialColor");

		if (vertexColorLocation != -1)
		{
			glUseProgram(programIndex);

			glUniform4f(vertexColorLocation, color.r, color.g, color.b, color.a);

			glUseProgram(0);
		}
		else
			std::cout << "MaterialColor not found ..." << std::endl;
	}
}*/

auto MeshRenderer::draw() const -> void
{
	if (programIndex != 0 && *vertexShader != nullptr && *fragmentShader != nullptr)
	{
		if (wireframe)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glUseProgram(programIndex);

		if (*material != nullptr)
		{
			MaterialData* matData = (MaterialData*)material->GetData();
			MaterialMetadata* matMetadata = (MaterialMetadata*)material->GetMetadata();

			GLint vertexColorLocation = glGetUniformLocation(programIndex, "materialColor");

			if (vertexColorLocation != -1)
				glUniform4f(vertexColorLocation, matData->color.r, matData->color.g, matData->color.b, matData->color.a);
			else
				std::cout << "MaterialColor not found ..." << std::endl;

			glBindTexture(GL_TEXTURE_2D, ((TextureMetadata*)matData->texture->GetMetadata())->texId);
		}

		ModelMetadata* meta = (ModelMetadata*)meshData->GetMetadata();

		glBindVertexArray(meta->vao);

		if (meta->ebo != 0)
			glDrawElements(GL_TRIANGLES, meta->indexCount, GL_UNSIGNED_INT, 0);
		else
			glDrawArrays(GL_TRIANGLES, 0, meta->vertexCount);

		glBindVertexArray(0);

		glBindTexture(GL_TEXTURE_2D, 0);

		glUseProgram(0);

		glPolygonMode(GL_FRONT, GL_FILL);
	}
}

auto MeshRenderer::UpdateShaders() -> void
{
	glAttachShader(programIndex, ((ShaderMetadata*)((AssetShader*)*vertexShader)->GetMetadata())->index);
	glAttachShader(programIndex, ((ShaderMetadata*)((AssetShader*)*fragmentShader)->GetMetadata())->index);
	glLinkProgram(programIndex);

	GLint success;
	char infoLog[512];

	glGetProgramiv(programIndex, GL_LINK_STATUS, &success);
	if (!success) 
	{
		glGetProgramInfoLog(programIndex, 512, NULL, infoLog);
		std::cout << "Link Error : " << infoLog << std::endl;
		return;
	}
	//SetColor(color);
}
