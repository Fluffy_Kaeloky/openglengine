#pragma once
#include <vector>
#include "Asset.h"

class AssetManager
{
public:
	AssetManager() = delete;

	static auto CreateAsset(AssetType type, void* data, void* metadata) -> Asset*;
	static auto CreateAssetFromFile(AssetType type, const std::string& file) -> Asset*;

	static auto AddAsset(Asset* asset) -> Asset*;

	static auto GetAssetFromUUID(UUID uuid) -> Asset*;

private:
	static std::vector<Asset*> assets;
};