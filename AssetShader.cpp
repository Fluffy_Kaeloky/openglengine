#include "AssetShader.h"

AssetShader::AssetShader(const char* source, ShaderType type):
	Asset()
{
	this->type = AssetType::ASSET_SHADER;
	this->data = new ShaderData(source);
	this->metadata = new ShaderMetadata();

	((ShaderMetadata*)metadata)->index = glCreateShader(type == ShaderType::SHADER_FRAGMENT ? GL_FRAGMENT_SHADER : GL_VERTEX_SHADER);
	((ShaderMetadata*)metadata)->type = type;
	glShaderSource(((ShaderMetadata*)metadata)->index, 1, &((ShaderData*)data)->source, nullptr);
	glCompileShader(((ShaderMetadata*)metadata)->index);

	glGetShaderiv(((ShaderMetadata*)metadata)->index, GL_COMPILE_STATUS, &((ShaderMetadata*)metadata)->compileSuceeded);
	if (!((ShaderMetadata*)metadata)->compileSuceeded)
		glGetShaderInfoLog(((ShaderMetadata*)metadata)->index, 512, nullptr, ((ShaderMetadata*)metadata)->compilationLog);
}

AssetShader::~AssetShader()
{
	delete data;
	delete metadata;
}

ShaderData::ShaderData(const char* source)
{
	unsigned int len = strlen(source);
	this->source = new char[len + 1];
	memcpy(this->source, source, len);
	this->source[len] = '\0';
}

ShaderData::~ShaderData()
{
	if (source)
		delete source;
	source = nullptr;
}

ShaderMetadata::~ShaderMetadata()
{
	glDeleteShader(index);
}
