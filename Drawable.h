#pragma once
#include "Renderer.h"

class Drawable
{
	friend class Renderer;

private:
	virtual auto draw() const -> void = 0;
};