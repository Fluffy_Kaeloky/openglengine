#version 330 core

in vec3 vertexColor;
in vec2 TexCoord;

out vec4 outColor;

uniform vec4 materialColor;

uniform sampler2D textureSampler;

void main()
{
	 outColor = vec4(vertexColor, 1.0) * materialColor * texture(textureSampler, TexCoord);
}