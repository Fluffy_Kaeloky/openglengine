#include "Renderer.h"

#include <stdlib.h>
#include <iostream>
#include <GL\glew.h>
#include "GlError.h"
#include "Drawable.h"

auto Renderer::Initialize(int vpul, int vpur, int vpbl, int vpbr) -> int
{
	if (!initialized)
	{
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			std::cout << "Couldn't intialize GLEW." << std::endl;
			return EXIT_FAILURE;
		}

		GetError();

		glViewport(vpul, vpur, vpbl, vpbr);
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		initialized = true;

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}

auto Renderer::Shutdown() -> void
{
	if (initialized)
	{
		initialized = false;
	}
}

auto Renderer::Draw(Drawable* drawable) -> void
{
	drawable->draw();
}

auto Renderer::SetClearColor(float r, float g, float b, float a) -> void
{
	glClearColor(r, g, b, a);
}

auto Renderer::SetViewport(int vpul, int vpur, int vpbl, int vpbr) -> void
{
	glViewport(vpul, vpur, vpbl, vpbr);
}
