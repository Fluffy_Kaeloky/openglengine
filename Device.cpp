#include "Device.h"
#include <iostream>

#include "Input.h"
#include "Renderer.h"
#include "GlError.h"

////////////////////
//TEST INCLUDES
#include "AssetManager.h"
#include "ModelAsset.h"
#include "AssetShader.h"
#include "MeshRenderer.h"
#include "MaterialAsset.h"
#include "TextureAsset.h"
#include <fstream>
#include <string>
////////////////////

Device::~Device()
{
	if (initialized)
		Shutdown();
}

auto Device::Initialize(int width, int height, const std::string & title) -> int
{
	if (!initialized)
	{
		glfwInit();

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
		if (!window)
		{
			std::cout << "Couldn't create GLFW Window." << std::endl;
			glfwTerminate();
			return EXIT_FAILURE;
		}

		glfwMakeContextCurrent(window);
		
		renderer = new Renderer();
		renderer->Initialize(0, 0, width, height);

		input = new Input();
		input->Initialize(this);

		initialized = true;


		/////////////////
		//TEST ZONE

		/*float triangle[] = { -0.5f, -0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			0.0f,  0.5f, 0.0f };*/

		/*GLfloat vertices[] = {
			0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
			0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 
			-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
			-0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 0.0f
		};*/

		GLfloat vertices[] = {
			0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
			-0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
			-0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f
		};

		GLuint indices[] = {
			0, 1, 3,
			1, 2, 3 
		};

		std::ifstream  vsifs("./VertexShader.glsl");
		std::string vertexShaderSource((std::istreambuf_iterator<char>(vsifs)), (std::istreambuf_iterator<char>()));

		std::ifstream  fsifs("./FragmentShader.glsl");
		std::string fragmentShaderSource((std::istreambuf_iterator<char>(fsifs)), (std::istreambuf_iterator<char>()));

		MaterialAsset* defaultMaterial = (MaterialAsset*)AssetManager::AddAsset(new MaterialAsset());
		defaultMaterial->SetColor(Color4f(0.8f, 0.8f, 0.8f, 1.0));

		MaterialAsset* containerMaterial = (MaterialAsset*)AssetManager::AddAsset(new MaterialAsset());
		TextureAsset* texAsset = (TextureAsset*)AssetManager::AddAsset(new TextureAsset("./container.jpg"));
		containerMaterial->SetTexture(texAsset);

		meshRenderer = new MeshRenderer();
		ModelAsset* triangleAsset = (ModelAsset*)AssetManager::AddAsset(new ModelAsset(vertices, 4, 8, 0));
		triangleAsset->SetAttribute(0, 3, GL_FLOAT, 0, 8);
		triangleAsset->SetAttribute(1, 3, GL_FLOAT, 3, 8);
		triangleAsset->SetAttribute(2, 2, GL_FLOAT, 6, 8);
		triangleAsset->SetIndexArray(indices, 6);

		meshRenderer->SetMesh(triangleAsset);
		meshRenderer->SetMaterial(containerMaterial);

		meshRenderer->SetVertexShader(AssetManager::AddAsset(new AssetShader(vertexShaderSource.c_str(), ShaderType::SHADER_VERTEX)));
		meshRenderer->SetFragmentShader(AssetManager::AddAsset(new AssetShader(fragmentShaderSource.c_str(), ShaderType::SHADER_FRAGMENT)));

		//End Test zone

		return EXIT_SUCCESS;
	}

	std::cout << "Device is already intialized." << std::endl;

	return EXIT_FAILURE;
}

auto Device::Shutdown() -> void
{
	if (initialized)
	{
		if (input)
		{
			input->Shutdown();
			delete input;
		}
		input = nullptr;

		if (renderer)
		{
			renderer->Shutdown();
			delete renderer;
		}
		renderer = nullptr;

		glfwTerminate();
		initialized = false;
	}
}

auto Device::Run() -> void
{
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		renderer->Draw(meshRenderer);

		//GetError();

		glfwSwapBuffers(window);
	}
}
