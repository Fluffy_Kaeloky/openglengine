#include "GlError.h"

auto GetError() -> GLenum
{
	GLenum error = GL_NO_ERROR;

	if ((error = glGetError()) != GL_NO_ERROR)
	{
		const GLubyte* strError = gluErrorString(error);
		std::cout << "An error happend : " << strError << std::endl;
	}

	return error;
}