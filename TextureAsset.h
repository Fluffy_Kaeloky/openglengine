#pragma once
#include "Asset.h"
#include <string>
#include <GL\glew.h>

enum class TextureFilterType
{
	MIGNIFICATION,
	MAGNIFICATION
};

enum class TextureFilter
{
	LINEAR,
	NEAREST
};

enum class TextureRepeatMode
{
	REPEAT,
	MIRRORED_REPEAT,
	CLAMP_TO_EDGE,
	CLAMP_TO_BORDER
};

class TextureAsset:
	public Asset
{
public:
	TextureAsset(const std::string& filename);
	virtual ~TextureAsset();

	auto SetFilter(TextureFilterType type, TextureFilter filter) -> void;
};

struct TextureData
{
	TextureData(unsigned char* data, unsigned int size);
	~TextureData();

	unsigned char* data = nullptr;
};

struct TextureMetadata
{
	~TextureMetadata();

	int width = 0;
	int height = 0;

	GLuint texId = 0;

	TextureFilter magFilter = TextureFilter::NEAREST;
	TextureFilter minFilter = TextureFilter::NEAREST;
};