#pragma once

class Drawable;

class Renderer
{
public:
	Renderer() = default;

	auto Initialize(int vpul, int vpur, int vpbl, int vpbr) -> int;
	auto Shutdown() -> void;

	auto Draw(Drawable*) -> void;

	auto SetClearColor(float r, float g, float b, float a = 1.0f) -> void;
	auto SetViewport(int vpul, int vpur, int vpbl, int vpbr) -> void;

private:
	bool initialized = false;
};