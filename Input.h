#pragma once

struct GLFWwindow;

class Device;

class Input
{
public:
	Input() = default;
	
	auto Initialize(Device* device) -> int;
	auto Shutdown() -> void;

private:
	static auto KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) -> void;

private:
	bool initialized = false;
	Device* device = nullptr;
};