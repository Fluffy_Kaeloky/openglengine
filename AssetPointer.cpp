#include "AssetPointer.h"
#include "AssetManager.h"

auto AssetPointer::operator->() const -> Asset *
{
	return *(*this);
}

auto AssetPointer::operator*() const -> Asset *
{
	if (!resolved)
		Resolve();
	return resolvedPointer;
}

auto AssetPointer::operator = (Asset* other) -> const AssetPointer&
{
	uuid = other->GetId();
	resolved = true;
	resolvedPointer = other;

	return *this;
}

auto AssetPointer::operator = (const AssetPointer& other) -> const AssetPointer&
{
	uuid = other.uuid;
	return *this;
}

auto AssetPointer::Set(Asset* other) -> void
{
	*this = other;
}

auto AssetPointer::Resolve() -> void
{
	resolvedPointer = AssetManager::GetAssetFromUUID(uuid);
	resolved = true;
}

auto AssetPointer::Resolve() const -> Asset *
{
	(Asset*)resolvedPointer = AssetManager::GetAssetFromUUID(uuid);
	(bool)resolved = true;
	return resolvedPointer;
}
