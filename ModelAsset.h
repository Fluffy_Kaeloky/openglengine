#pragma once
#include "Asset.h"
#include <gl\glew.h>

class ModelAsset :
	public Asset
{
public:
	ModelAsset(float* vertex, int vertexCount, int vertexSize, int stride = 0);
	virtual ~ModelAsset();

	auto SetAttribute(GLuint layout, GLuint attribSize, GLenum type, GLint offset, GLint stride, bool normalize = false) -> void;
	auto SetIndexArray(unsigned int* indices, unsigned int count) -> void;
};

struct ModelData
{
	ModelData(float* vertex, int byteSize);
	~ModelData();

	float* vertex = nullptr;
	float* indices = nullptr;
};

struct ModelMetadata
{
	~ModelMetadata();

	int vertexCount = 0;
	int vertexSize = 0;
	int stride = 0;

	int indexCount = 0;

	GLuint vbo = 0;
	GLuint vao = 0;
	GLuint ebo = 0;
};
